<?php

namespace CodePress\CodeCategory\Tests\Controllers;


use CodePress\CodeCategory\Controllers\AdminCategoriesController;
use CodePress\CodeCategory\Controllers\Controller;
use CodePress\CodeCategory\Repository\CategoryRepository;
use CodePress\CodeCategory\Tests\AbstractTestCase;
use Illuminate\Contracts\Routing\ResponseFactory;
use Mockery as m;

class AdminCategoriesControllerTest extends AbstractTestCase
{
    public function test_should_extends_controller()
    {
        $repository = m::mock(CategoryRepository::class);
        $responseFactory = m::mock(ResponseFactory::class);
        $controller = new AdminCategoriesController($responseFactory, $repository);

        $this->assertInstanceOf(Controller::class, $controller);

    }
}