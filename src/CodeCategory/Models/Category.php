<?php

namespace CodePress\CodeCategory\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Category extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $table = "codepress_categories";

    protected $sluggable = [
        'build_from' => 'name',
        'save_to' => 'slug',
        'unique' => true
    ];

    protected $fillable = [
        'name',
        'slug',
        'active',
        'parent_id'
    ];

    protected $casts = [
        'active' => 'boolean'
    ];

    private $validator;

    public function setValidator($validator)
    {
        $this->validator = $validator;
    }

    public function getValidator()
    {
        return $this->validator;
    }

    public function isValid()
    {
        $validator = $this->validator;
        $validator->setRules(['name' => 'required|max:255']);
        $validator->setData($this->attributes);
        return !$validator->fails();
    }

    public function categorizable()
    {
        return $this->morphTo();
    }

    public function parent()
    {
        return $this->belongsTo(Category::class);
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function setParentIdAttribute($value)
    {
        if(empty($value)){
            $this->attributes['parent_id'] = null;
        }else{
            $this->attributes['parent_id'] = $value;
        }

    }

    public function setActiveAttribute($value)
    {
        if(empty($value)){
            $this->attributes['active'] = false;
        }else{
            $this->attributes['active'] = true;
        }

    }
}
