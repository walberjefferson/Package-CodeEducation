<?php
/**
 * Created by PhpStorm.
 * User: Binho
 * Date: 29/08/2016
 * Time: 16:31
 */

namespace CodePress\CodeCategory\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}