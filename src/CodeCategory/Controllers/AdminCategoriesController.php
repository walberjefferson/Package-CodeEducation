<?php

namespace CodePress\CodeCategory\Controllers;

use CodePress\CodeCategory\Repository\CategoryRepository;
use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory;

class AdminCategoriesController extends Controller
{
    private $repository;
    private $response;

    public function __construct(ResponseFactory $response, CategoryRepository $repository)
    {
        $this->repository = $repository;
        $this->response = $response;
    }
    
    public function index()
    {
        $categories = $this->repository->all();
        return $this->response->view('codecategory::index', compact('categories'));
    }

    public function create()
    {
        $categories = $this->repository->all();
        return view('codecategory::create', compact('categories'));
    }

    public function store(Request $request)
    {
        #dd($request->all());
        $this->repository->create($request->all());
        return redirect()->route('admin.categories.index');
    }

    public function edit($id)
    {
        $category = $this->repository->find($id);
        $categories = $this->repository->all();
        return view('codecategory::edit', compact('category', 'categories'));
    }

    public function update(Request $request, $id)
    {

        $parent_id = $request->input("parent_id");
        $name = $request->input("name");
        $slug = $request->input("slug");
        $status = $request->input("active");


        if (empty($status)) {
            $status = false;
        } else {
            $status = true;
        }
        //dd($parent_id);
        $result = $this->repository->find($id);
        //dd($result->toArray());
        //$result->fill($request->all())->save();
        $result->fill(['parent_id' => $parent_id, 'name' => $name, 'slug' => $slug, 'active' => $status])->save();
        return redirect()->route('admin.categories.index');

    }

}