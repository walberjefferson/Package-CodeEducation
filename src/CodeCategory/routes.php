<?php

Route::group(['prefix' => 'admin/categories', 'as' => 'admin.categories.', 'namespace' => 'CodePress\CodeCategory\Controllers', 'middleware' => ['web']], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'AdminCategoriesController@index']);
    Route::get('/create', ['as' => 'create', 'uses' => 'AdminCategoriesController@create']);
    Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'AdminCategoriesController@edit']);
    Route::post('/store', ['as' => 'store', 'uses' => 'AdminCategoriesController@store']);
    Route::post('/update/{id}', ['as' => 'update', 'uses' => 'AdminCategoriesController@update']);
});