@extends('layouts.app')

@section('content')
    <div class="container">
        <h3>Categories</h3>
        <a href="{{route('admin.categories.create')}}">Create Category</a>
        <br><br>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th width="3%" class="text-center">ID</th>
                <th>Nome</th>
                <th width="6%" class="text-center">Status</th>
                <th width="6%" class="text-center">Ações</th>
            </tr>
            </thead>

            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td class="text-center">{{$category->id}}</td>
                    <td>{{$category->name}}</td>
                    <td class="text-center">
                        @if($category->active)
                            <span class="label label-success ">Ativo</span>
                        @else
                            <span class="label label-danger">Inativo</span>
                        @endif
                    </td>
                    <td class="text-center">
                        <a href="{{ route('admin.categories.edit', ['id'=>$category->id]) }}" class="text-success"><span
                                    class="glyphicon glyphicon-pencil"></span></a>
                        <a href="#" class="text-danger"><span class="glyphicon glyphicon-trash"></span></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection
