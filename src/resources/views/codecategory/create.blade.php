@extends('layouts.app')

@section('content')
    <div class="container">
        <h3>Create Category</h3>

        {!! Form::open(['method' => 'post', 'route' => 'admin.categories.store']) !!}

        <div class="form-group">
            {!! Form::label('parent_id', 'Categorias:') !!}
            <select name="parent_id" class="form-control">
                <option value="" selected>- Nenhum -</option>
                @foreach($categories as $categoy)
                    <option value="{{$categoy->id}}">{{$categoy->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            {!! Form::label('name', 'Nome:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('active', 'Status:') !!}
            {!!  Form::checkbox('active', '1', true)  !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
        </div>

        {!! Form::close() !!}

    </div>

@endsection